

directory = '/media/kpc/Desktop/SLP_KINECT/data/pointclouds/'
#num_files = 0

#capture = True
#capture = False
show = False

capture = True
#show = True


#loops = 9000
#loops = 60   #SET to 1 for reading pcd files
loops = 2  #Set to 30 (or any #) for capturing pcd files

#3D-Visualizer

import open3d as o3d
#Kinect
import pylibfreenect2

from pylibfreenect2 import Freenect2
from pylibfreenect2 import SyncMultiFrameListener
from pylibfreenect2 import Registration
from pylibfreenect2 import Frame
from pylibfreenect2 import FrameType
import pandas as pd
import numpy as np
import pcl
import time
from time import sleep

import matplotlib.pyplot as plt

import cv2
import sys
import os
import PyQt5
from pyqtgraph.Qt import QtCore, QtGui

import trimesh
import open3d.visualization.gui as gui
import random
import ktb


fn = Freenect2()
num_devices = fn.enumerateDevices()
if num_devices == 0:
    sys.exit(1)

serial = fn.getDeviceSerialNumber(0)
device = fn.openDevice(serial)

framecount = 0

types = 0
types |= FrameType.Color
types |= (FrameType.Ir | FrameType.Depth)
listener = SyncMultiFrameListener(types)

# Register listeners
device.setColorFrameListener(listener)
device.setIrAndDepthFrameListener(listener)

device.start()
#viewer = o3d.visualization.



# NOTE: must be called after device.start()
registration = Registration(device.getIrCameraParams(),
                            device.getColorCameraParams())

undistorted = Frame(512, 424, 4)
registered = Frame(512, 424, 4)


# Kinects's intrinsic parameters based on v2 hardware (estimated).
CameraParams = {
  "cx":254.878,
  "cy":205.395,
  "fx":365.456,
  "fy":365.456,
  "k1":0.0905474,
  "k2":-0.26819,
  "k3":0.0950862,
  "p1":0.0,
  "p2":0.0,
}




def from_dir():    
    for filename in os.listdir(directory):
        if filename.endswith(".pcd") or filename.endswith(".obj"):  
            #searching directory for .pcd or .obj files
            print(os.path.join(directory, filename))
            clouds = list() 
                                                                    #Saving the PCD files into memory as an array of clouds(ideally)
                                                                    #!!!
                                                                    #a cloud being a .pcd (or other 3D object file extension, obj for example) file
                                                                    #!!!


                                                                    #FOR READING FROM DIRECTORY
            curr_cloud = os.path.join(directory, filename)
            curr_pcd = o3d.io.read_point_cloud(curr_cloud)          #FOR READING FROM CAM
            clouds.append(curr_pcd)
                                                                    #Error Checking - Print files loaded into clouds
        #print(clouds)

                                                                    #VISUALIZE FILES IN DIRECTORY
                                                                    #that are now in clouds list        
            return clouds



def depthToPointCloudPos(x_d, y_d, z, scale=1000):
    # This runs in Python slowly as it is required to be called from within a loop, but it is a more intuitive example than it's vertorized alternative (Purly for example)
    # calculate the real-world xyz vertex coordinate from the raw depth data (one vertex at a time).
    x = (x_d - CameraParams['cx']) * z / CameraParams['fx']
    y = (y_d - CameraParams['cy']) * z / CameraParams['fy']

    return x / scale, y / scale, z / scale

def depthMatrixToPointCloudPos(z, scale=1000):
    # bacically this is a vectorized version of depthToPointCloudPos()
    # calculate the real-world xyz vertex coordinates from the raw depth data matrix.
    C, R = np.indices(z.shape)

    R = np.subtract(R, CameraParams['cx'])
    R = np.multiply(R, z)
    R = np.divide(R, CameraParams['fx'] * scale)

    C = np.subtract(C, CameraParams['cy'])
    C = np.multiply(C, z)
    C = np.divide(C, CameraParams['fy'] * scale)

    return np.column_stack((z.ravel() / scale, R.ravel(), -C.ravel()))

# Kinect's physical orientation in the real world.
CameraPosition = {
    "x": 0, # actual position in meters of kinect sensor relative to the viewport's center.
    "y": 2, # actual position in meters of kinect sensor relative to the viewport's center.
    "z": 1.7, # height in meters of actual kinect sensor from the floor.
    "roll": 0, # angle in degrees of sensor's roll (used for INU input - trig function for this is commented out by default).
    "azimuth": 0, # sensor's yaw angle in degrees.
    "elevation": -15, # sensor's pitch angle in degrees.
}

def applyCameraOrientation(pt):
    # Kinect Sensor Orientation Compensation
    # This runs slowly in Python as it is required to be called within a loop, but it is a more intuitive example than it's vertorized alternative (Purly for example)
    # use trig to rotate a vertex around a gimbal.
    def rotatePoints(ax1, ax2, deg):
        # math to rotate vertexes around a center point on a plane.
        hyp = np.sqrt(pt[ax1] ** 2 + pt[ax2] ** 2) # Get the length of the hypotenuse of the real-world coordinate from center of rotation, this is the radius!
        d_tan = np.arctan2(pt[ax2], pt[ax1]) # Calculate the vertexes current angle (returns radians that go from -180 to 180)

        cur_angle = np.degrees(d_tan) % 360 # Convert radians to degrees and use modulo to adjust range from 0 to 360.
        new_angle = np.radians((cur_angle + deg) % 360) # The new angle (in radians) of the vertexes after being rotated by the value of deg.

        pt[ax1] = hyp * np.cos(new_angle) # Calculate the rotated coordinate for this axis.
        pt[ax2] = hyp * np.sin(new_angle) # Calculate the rotated coordinate for this axis.

    #rotatePoints(0, 2, CameraPosition['roll']) #rotate on the Y&Z plane # Disabled because most tripods don't roll. If an Inertial Nav Unit is available this could be used)
    rotatePoints(1, 2, CameraPosition['elevation']) #rotate on the X&Z plane
    rotatePoints(0, 1, CameraPosition['azimuth']) #rotate on the X&Y plane

    # Apply offsets for height and linear position of the sensor (from viewport's center)
    pt[:] += np.float_([CameraPosition['x'], CameraPosition['y'], CameraPosition['z']])



    return pt

def applyCameraMatrixOrientation(pt):
    # Kinect Sensor Orientation Compensation
    # bacically this is a vectorized version of applyCameraOrientation()
    # uses same trig to rotate a vertex around a gimbal.
    def rotatePoints(ax1, ax2, deg):
        # math to rotate vertexes around a center point on a plane.
        hyp = np.sqrt(pt[:, ax1] ** 2 + pt[:, ax2] ** 2) # Get the length of the hypotenuse of the real-world coordinate from center of rotation, this is the radius!
        d_tan = np.arctan2(pt[:, ax2], pt[:, ax1]) # Calculate the vertexes current angle (returns radians that go from -180 to 180)

        cur_angle = np.degrees(d_tan) % 360 # Convert radians to degrees and use modulo to adjust range from 0 to 360.
        new_angle = np.radians((cur_angle + deg) % 360) # The new angle (in radians) of the vertexes after being rotated by the value of deg.

        pt[:, ax1] = hyp * np.cos(new_angle) # Calculate the rotated coordinate for this axis.
        pt[:, ax2] = hyp * np.sin(new_angle) # Calculate the rotated coordinate for this axis.

    #rotatePoints(1, 2, CameraPosition['roll']) #rotate on the Y&Z plane # Disabled because most tripods don't roll. If an Inertial Nav Unit is available this could be used)
    rotatePoints(0, 2, CameraPosition['elevation']) #rotate on the X&Z plane
    rotatePoints(0, 1, CameraPosition['azimuth']) #rotate on the X&Y

    # Apply offsets for height and linear position of the sensor (from viewport's center)
    pt[:] += np.float_([CameraPosition['x'], CameraPosition['y'], CameraPosition['z']])



    return pt


def main():
    colors = ((1.0, 1.0, 1.0, 1.0))



    frames = listener.waitForNewFrame()

    # Get the frames from the Kinect sensor
    ir = frames["ir"]
    color = frames["color"]
    depth = frames["depth"]

    d = depth.asarray() #the depth frame as an array (Needed only with non-vectorized functions)

    registration.apply(color, depth, undistorted, registered)

    # Format the color registration map - To become the "color" input for the scatterplot's setData() function.
    colors = registered.asarray(np.uint8)
    colors = np.divide(colors, 255) # values must be between 0.0 - 1.0
    colors = colors.reshape(colors.shape[0] * colors.shape[1], 4 ) # From: Rows X Cols X RGB -to- [[r,g,b],[r,g,b]...]
    colors = colors[:, :3:]  # remove alpha (fourth index) from BGRA to BGR
    colors = colors[...,::-1] #BGR to RGB

    # Calculate a dynamic vertex size based on window dimensions and camera's position - To become the "size" input for the scatterplot's setData() function.
    v_rate = 5.0 # Rate that vertex sizes will increase as zoom level increases (adjust this to any desired value).
    #v_scale = np.float32(v_rate) / gl_widget.opts['distance'] # Vertex size increases as the camera is "zoomed" towards center of view.
    #v_offset = (gl_widget.geometry().width() / 1000)**2 # Vertex size is offset based on actual width of the viewport.
    #v_size = v_scale + v_offset
    v_size=0.5
    # Calculate 3d coordinates (Note: five optional methods are shown - only one should be un-commented at any given time)

    '''
    # Method 1 (No Processing) - Format raw depth data to be displayed
    m, n = d.shape
    R, C = np.mgrid[:m, :n]
    out = np.column_stack((d.ravel() / 4500, C.ravel()/m, (-R.ravel()/n)+1))

    '''
    # Method 2 (Fastest) - Format and compute the real-world 3d coordinates using a fast vectorized algorithm - To become the "pos" input for the scatterplot's setData() function.
    out = depthMatrixToPointCloudPos(undistorted.asarray(np.float32))


    '''
    # Method 3 - Format undistorted depth data to real-world coordinates
    n_rows, n_columns = d.shape
    out = np.zeros((n_rows * n_columns, 3), dtype=np.float32)
    for row in range(n_rows):
        for col in range(n_columns):
            z = undistorted.asarray(np.float32)[row][col]
            X, Y, Z = depthToPointCloudPos(row, col, z)
            out[row * n_columns + col] = np.array([Z, Y, -X])
    '''
    '''
    # Method 4 - Format undistorted depth data to real-world coordinates
    n_rows, n_columns = d.shape
    out = np.zeros((n_rows * n_columns, 3), dtype=np.float64)
    for row in range(n_rows):
        for col in range(n_columns):
            X, Y, Z = registration.getPointXYZ(undistorted, row, col)
            out[row * n_columns + col] = np.array([Z, X, -Y])

    '''



    # Method 5 - Format undistorted and regisered data to real-world coordinates with mapped colors (dont forget color=colors in setData)
    '''
    n_rows, n_columns = d.shape
    out = np.zeros((n_rows * n_columns, 3), dtype=np.float64)
    colors = np.zeros((d.shape[0] * d.shape[1], 3), dtype=np.float64)
    for row in range(n_rows):
        for col in range(n_columns):
            X, Y, Z, B, G, R = registration.getPointXYZRGB(undistorted, registered, row, col)
            out[row * n_columns + col] = np.array([Z, X, -Y])
            colors[row * n_columns + col] = np.divide([R, G, B], 255)
    '''


    # Kinect sensor real-world orientation compensation.
    out = applyCameraMatrixOrientation(out)

    '''                                                                    #Saving pointcloud after camera orientation comp.
        print("Testing IO for point cloud ...")

        pcd = o3d.geometry.PointCloud()                                 #binding for [x,y,z]    ----->  POINTCLOUD
        pcd.points = o3d.utility.Vector3dVector(out)                    #setting points using 3D-Vectors
        pcd.colors = o3d.utility.Vector3dVector(colors)
        distances = pcd.compute_nearest_neighbor_distance()
        avg_dist = np.mean(distances)
        radius = 1.5 * avg_dist


        pcd.estimate_normals()

        viz(pcd)
    '''
    # uni_down_pcd.estimate_normals()
    ''' print('run Poisson surface reconstruction')
        with o3d.utility.VerbosityContextManager(
                o3d.utility.VerbosityLevel.Debug) as cm:
            mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
                uni_down_pcd, depth=9)
    '''  # print(mesh)
    '''o3d.visualization.draw_geometries([mesh],
                                    zoom=0.664,
                                    front=[-0.4761, -0.4698, -0.7434],
                                    lookat=[1.8900, 3.2596, 0.9284],
                                    up=[0.2304, -0.8825, 0.4101])
    '''
    # print('remove low density vertices')
    # vertices_to_remove = densities < np.quantile(densities, 0.01)
    # mesh.remove_vertices_by_mask(vertices_to_remove)
    # print(mesh)
    # uni_down_pcd.orient_normals_consistent_tangent_plane(100)
    # o3d.visualization.draw_geometries([pcd],
    #                                zoom=.5,
    #                                front=[0, 0, 2],
    #                                lookat=[0, 0, 0],
    #                                up=[0.25, 0, 0.5])
    #mesh = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(pcd,o3d.utility.DoubleVector([radius, radius * 2]))
    #tri_mesh = trimesh.Trimesh(np.asarray(mesh.vertices), np.asarray(mesh.triangles),vertex_normals=np.asarray(mesh.vertex_normals))

    #trimesh.convex.is_convex(mesh)

    #print(tri_mesh)

    # Update estimation
    # viewer.removePointCloud(b'normals')
    # viewer.RemovePointCloud(b'normals', 0)
    # viewer.addPointCloudNormals<PointType, pcl::Normal>( cloud, cloud_normals, 100, 0.05, "normals")
    # viewer.AddPointCloudNormals(cloud, cloud_normals, 100, 0.05, b'normals')

    #framecount = framecount + 1
    i = framecount


    #-------------------------------------------------------------------------------------
    #uncomment below to save out as .csv
    '''
    df = pd.DataFrame(out)
    path = next_path(r'/home/kpc/Desktop/SLP_KINECT/data/pointclouds/%s-cloud.csv')
    df.to_csv(path, index = True)

    print("CSV SAVED at "+path+"!!!"+"\n")

    '''
    #-------------------------------------------------------------------------------------
    #uncomment below to save as .pcd
    #path = next_path('/media/kpc/923GB WD/Kinect2/PCD_Files/PCD_file-%s.pcd')       #using most efficient file-iterating algorithm I could find
    #o3d.io.write_point_cloud(path, pcd)                             #SAVING PCD

    #print("POINTCLOUD (pcd file) SAVED!!!\n",path)
    #-------------------------------------------------------------------------------------
    #uncomment below to save as .obj    NOT WORKING
    #path = next_path('/media/kpc/923GB WD/Kinect2/PCD_Files/PCD_file-%s.obj')

    #uni_down_pcd.estimate_normals()

    #print('run Poisson surface reconstruction')
    #    with o3d.utility.VerbosityContextManager(
    #            o3d.utility.VerbosityLevel.Debug) as cm:
    #        mesh, densities = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(
    #            uni_down_pcd, depth=9)

    #print(mesh)

    #o3d.visualization.draw_geometries([mesh],
    #                                zoom=0.664,
    #                                front=[-0.4761, -0.4698, -0.7434],
    #                                lookat=[1.8900, 3.2596, 0.9284],
    #                                up=[0.2304, -0.8825, 0.4101])

    #print('remove low density vertices')
    #vertices_to_remove = densities < np.quantile(densities, 0.01)
    #mesh.remove_vertices_by_mask(vertices_to_remove)
    #print(mesh)
    #uni_down_pcd.orient_normals_consistent_tangent_plane(100)
    #o3d.visualization.draw_geometries([pcd],
    #                                zoom=.5,
    #                                front=[0, 0, 2],
    #                                lookat=[0, 0, 0],
    #                                up=[0.25, 0, 0.5])


    #o3d.io.write_point_cloud(path, pcd)                             #SAVING OBJ

    #print("OBJ FILE SAVED!!!\n",path)                                                                                #path automatically finds the next number to put
                                                                                    #   after a filename based on contents of
                                                                                    #   folder and name of file
    #uncomment below to enable capture of PCD or other filetypes with pointcloud data
    #if show:
    #viz(tri_mesh)

    #viewer.draw_geometries([pcd])
    recordKinect()
    listener.release(frames)


def plotClouds(c):
    plt.plot(c)

def next_path(path_pattern):
    """
    Finds the next free path in an sequentially named list of files

    e.g. path_pattern = 'file-%s.txt':

    file-1.txt
    file-2.txt
    file-3.txt

    Runs in log(n) time where n is the number of existing files in sequence
    """
    i = 1

    # First do an exponential search
    while os.path.exists(path_pattern % i):
        i = i * 2
        


    # Result lies somewhere in the interval (i/2..i]
    # We call this interval (a..b] and narrow it down until a + 1 = b
    a, b = (i // 2, i)
    while a + 1 < b:
        c = (a + b) // 2 # interval midpoint
        a, b = (c, b) if os.path.exists(path_pattern % c) else (a, c)

    return path_pattern % b    

def recordKinect():

        k = ktb.Kinect()

        timer = time.time()
        currTime = timer
        lastTime = currTime
        currTime = time.time()
        identification = "VideoCapture"

        path = next_path(r'/home/kpc/Desktop/SLP_KINECT/data/pics/'+identification+'%s'+'.avi')


        print("Video Saving to path: "+path+"!!!"+"\n")
        

        colorVideo = k.record(path)
        
        
        

if __name__ == "__main__":
    

    if (cv2.waitKey(1) & 0xFF == ord('q')):    
        time.sleep(1)        
        device.stop()
        time.sleep(2) 
        device.close()
        time.sleep(2) 
        sys.exit(0) 
    main()

    
