import KinectPV2.*; //<>//

import org.openkinect.processing.*;

// Kinect Library object
KinectPV2 kinect2;

float minThresh = 600;
float maxThresh = 1000;
PImage img;

void setup() {
  size(512, 424);
  kinect2 = new KinectPV2(this);
  kinect2.enableDepthImg(true);
  kinect2.enableInfraredImg(true);
  
  kinect2.init();
  img = createImage(kinect2.WIDTHDepth, kinect2.HEIGHTDepth, RGB);
}


void draw() {
  background(0);

  img.loadPixels();
  

  PImage dImg = kinect2.getDepthImage();
  PImage cImg = kinect2.getInfraredImage();
  
  cImg.loadPixels();
  
  //image(dImg, 0, 0);
  
  // Get the raw depth as array of integers
  int[] depth = kinect2.getRawDepthData();
  //color colors[];
  
  //int record = 4500;
  
  int record = kinect2.HEIGHTDepth;
  
  int rx = 0;
  int ry = 0;
  int lastposition=0;
  int nextposition=0;
  float deltaX=0;
  float recColor=0;
  float lastColor=0;
  float diff=0;
  for (int x = 0; x < kinect2.WIDTHDepth; x++) {
    for (int y = 0; y < kinect2.HEIGHTDepth; y++) {
      int offset = x + y * kinect2.WIDTHDepth;
      int d = depth[offset];
      float colors=0;
      
      //float lastColor; 
      

      if (d > minThresh && d < maxThresh) {
        img.pixels[offset] = color(255, 0, 150);
        nextposition = d;
        if (y < record) {
         
          
          //for (int cx = 0; cx < kinect2.WIDTHDepth; cx++) {
    //for (int cy = 0; cy < kinect2.HEIGHTDepth; cy++) {
          
          //if(cx!=0){
          //float lastColor = cImg.get(cx-1,cy)/10000;
          //}
          //if(recColor != lastColor){
          //recColor = cImg.get(cx,cy)/10000;}
          //}
          //}
          
          
          colors = cImg.get(rx,ry)/1000000;
          //print(recColor);
          lastColor = recColor;
          recColor = colors;
          diff = recColor-lastColor;
          if(( ( (diff<20)  &&  (diff>-20) )   ||   ( (diff<200)  &&  (diff>-200) ) ))
          //print(recColor);
          deltaX=(nextposition - lastposition);
          record = y;
          rx = x;
          ry = y;
          lastposition = depth[x+y*width];
          //delay(2);
          
          //print(cImg.get(rx*4,ry*4));
          if(  deltaX<=20 && deltaX>=-20 ) // && (deltaX < 300) && (deltaX>-300  ))
          {
            deltaX=0;
          
            
          }
        }
        
        
      } else {
        img.pixels[offset] = dImg.pixels[offset];
      }
       
       
    }
   
  }
  //cImg.updatePixels();
  img.updatePixels();
  image(img,0,0);
  
  fill(0);
  ellipse(rx, ry, 16, 16);
  textSize(14);
  text("color",100,100);
  text(recColor,100,200);
  text("speed",300,200);
  text((  ( deltaX )  /  0.033)  ,  300  ,  300);
}
