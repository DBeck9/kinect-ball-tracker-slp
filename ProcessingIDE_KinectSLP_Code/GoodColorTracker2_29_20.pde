/*
Bradley Decker

ACTUAL GOOD RED GOLFBALL TRACKER OMG FUCK YEA!

KinectPV2, Kinect for Windows v2 library for processing

Color to fepth Example,
Color Frame is aligned to the depth frame
*/

import KinectPV2.*;

KinectPV2 kinect;

boolean newP=false;
boolean newD=false;


boolean chk1=false;
boolean chk2=false;
boolean chk3=false;
boolean chk4=false;
boolean chk5=false;

boolean newMaxX=false;
boolean newMaxY=false;

boolean distChk=false;

int [] depthZero;

//BUFFER ARRAY TO CLEAN DE PIXLES
PImage depthToColorImg;

float valX;
float valY;

int valXDepth;
int valYDepth;

int minX;
int maxX;

int minY;
int maxY;

int lastD;

int valXColor;
int valYColor;

int w=1080;
int h=900;

int Coff;
int Doff;

int minR=80;
int maxR=160;

int minG=20;
int maxG=100;

int minB=20;
int maxB=100;

int lastCx;
int lastCy;

int  realX;
int  realY;

color colorPixel=color(0);
int [] depths;

void setup() {
  fullScreen(P2D);
  frameRate(60);
  depthToColorImg = createImage(512, 424, PImage.RGB);
  depthZero    = new int[ KinectPV2.WIDTHDepth * KinectPV2.HEIGHTDepth];
  depths = new int [512*424];
  //SET THE ARRAY TO 0s
  for (int i = 0; i < KinectPV2.WIDTHDepth; i++) {
    for (int j = 0; j < KinectPV2.HEIGHTDepth; j++) {
      depthZero[424*i + j] = 0;

    }
  }

  kinect = new KinectPV2(this);
  kinect.enableDepthImg(true);
  kinect.enableColorImg(true);
  kinect.enablePointCloud(true);

  kinect.init();
}

void draw() {
  background(0);
  color c=color(0);//for debugging
  float [] mapDCT = kinect.getMapDepthToColor();

  //get the raw data from depth and color
  int [] colorRaw = kinect.getRawColor();

  //get raw depths
  depths = kinect.getRawDepthData();
  //clean de pixels
  PApplet.arrayCopy(depthZero, depthToColorImg.pixels);

  int count = 0;
  depthToColorImg.loadPixels();
  for (int i = 0; i < KinectPV2.WIDTHDepth; i++) {


        
        
        
        
      Coff= (valYColor * 1920 + valXColor);
      Doff= (valXDepth * 424 + valYDepth); 
      
      

      
    
    for (int j = 0; j < KinectPV2.HEIGHTDepth; j++) {


      //incoming pixels 512 x 424 with position in 1920 x 1080
      valX = mapDCT[count * 2 + 0];
      valY = mapDCT[count * 2 + 1];


      //maps the pixels to 512 x 424, not necessary but looks better
      valXDepth = (int)((valX/1920.0) * 512.0);
      valYDepth = (int)((valY/1080.0) * 424.0);

      valXColor = (int)(valX);
      valYColor = (int)(valY);


      //wow
      if ( valXDepth >= 0 && valXDepth < 512 && valYDepth >= 0 && valYDepth < 424 &&
        valXColor >= 0 && valXColor < 1920 && valYColor >= 100 && valYColor < 1080) {
        
        colorPixel = colorRaw[valYColor * 1920 + valXColor];
        c=colorPixel;
        
        
        
        //color colorPixel = depthRaw[valYDepth*512 + valXDepth];
        if(red(c)>minR && red(c)>maxR && green(c)>minG && green(c)<maxG && blue(c)>minB && blue(c)<maxB )
        {
          

          //println("depths["+Doff+"] : "+lastD);
          //checkmaxX(valXColor, lastCx, valYColor, lastCy);
          //checkmaxY(valYColor, lastCy);
          depthToColorImg.pixels[valYDepth * 512 + valXDepth] = color(0,250,150);
                 //Coff= (valYDepth * 512 + valXDepth);
          if(newP)
          {
          lastCx = valXColor;
          lastCy = valYColor;
          checkD(depths[Doff],lastD);//checking if new position has new depth(prob does)
          lastD=depths[Doff];
          }
          

          
          

        }
        else
        {
          depthToColorImg.pixels[valYDepth * 512 + valXDepth] = colorPixel;
                  
        }
      }
      count++;
  


    }
  }
  //depthToColorImg.copy(depthToColorImg,0,0,1920,1080, 0, 0,1080,900);
  depthToColorImg.updatePixels();

  image(depthToColorImg,0,0,displayWidth,displayHeight);
  image(kinect.getColorImage(), 0, 0, 0, 0);
  image(kinect.getDepthImage(), 0, 0, 0, 0);
  constrain(realX,50,1920-50);
  constrain(realY,50,1080-50);
  //image(depthToColorImg,realX-50,realY-50,realX+50,realY+50);
  

//rect(realX-20,realY-5,realX+20,realY+5);

fill(255);
textSize(20);
  text("fps: "+frameRate, 50, 50);

strokeWeight(3);
fill(0,0,0);
/*
text("{R}  colorPixel[ "+Coff+"] :     "+red(c), (w*0.05), (h*0.6) + (30*0));
text("{G}  colorPixel[ "+Coff+"] :     "+green(c), (w*0.05), (h*0.6) + (30*2));
text("{B}  colorPixel[ "+Coff+"] :     "+blue(c), (w*0.05), (h*0.6) + (30*4));
text("{Full Color Hex}  colorPixel[ "+Coff+"] :     "+hex(c), (w*0.05), (h*0.6) + (30*6));


text("mouseX  =  "+mouseX+"      "+"mouseY  =  "+mouseY, (w*0.05), (h*0.6) + (30*8));

text("valXColor: "+valX+"      "+"ValYColor"+valY, (w*0.35), (h*0.6) + (30) );
//fill(0,255,0);
text("valXColor: "+valXColor+"      "+"ValYColor"+valYColor, (w*0.35), (h*0.6) + (30*2) );
//fill(0,0,255);
text("valXDepth: "+valXDepth+"      "+"ValYDepth"+valYDepth, (w*0.35), (h*0.6) + (30*4) );
*/
          if(chk1)
       {
         //println("Coff:"+Coff);
         if(newD)
       {
         println("depths[Doff]:"+depths[Doff]+"        "+"Coff:"+Coff);
         noFill();
         stroke(0);

       }
         ellipse(maxX,maxY,40,40);
         fill(0,255,0);
          ellipse(100,maxX,(depths[Doff]/100),(depths[Doff]/100));
            fill(0,0,255);
              ellipse(50,maxY,(depths[Doff]/100),(depths[Doff]/100));
       }   
      
}

void checkD(int d, int prevD)
{
/*if(d > prevD)
{
newD=true;
}
if(d < prevD)
{
newD=true;
}*/
if( abs(d - prevD) < 500 )
{
newD=true;
println("current d="+d+"      "+"last d="+prevD);
}
else
{
newD=false;
}


}

void mousePressed()
{
  
println("X="+mouseX+"    "+"Y="+mouseY+"    "+"newD:"+newD+"newP:"+newP);

}


void keyPressed(){

 if (key == 's') {
 
println("minX:"+minX);
println("maxX:"+maxX);

 
println("minY:"+minY);
println("maxY:"+maxY);


       chk1=!chk1;

 }
   if (key == 'q') {
println("Coff:"+Coff);


}
  if (key == 'r') {
      maxR+=10;
      println("maxR:"+maxR);
      if(maxR>=255)
      {
        maxR=minR+10;
      }  
}

  if (key == 'g') {
      maxG+=10;
      println("maxG:"+maxG);
      if(maxG>=255)
      {
        maxG=minG+10;

      }
  }

  if (key == 'b') {
      maxB+=10;
      println("maxB:"+maxB);
      if(maxB>=255)
      {
        maxB=minB+10;
      }
  }


  if (key == 'c') {
      println("");
      
      println("maxR:"+maxR);
      println("minR:"+minR);
      
      println("");
      
            println("maxG:"+maxG);
            println("minG:"+minG);            
      
      println("");
      
                  println("maxB:"+maxB);
                  println("minB:"+minB);
      
      println("");

} 
  
}
