import KinectPV2.*;

import gab.opencv.*;
import java.awt.*;

OpenCV opencv;
KinectPV2 kinect;
boolean distCHK=false;
boolean check1=false;
boolean check2=false;
boolean check3=false;
boolean check4=false;
boolean check5=false;
boolean check6=false;
int mouseOffsetC=0;
int mouseOffsetD=0;
int ratio = int(1440*900)/(  (kinect.WIDTHDepth)*(kinect.HEIGHTDepth)  );
int minR = 180;
int maxR = 200;
int minG = 180;
int maxG = 200;
int minB = 180;
int maxB = 200;

int minD = 500;
int maxD = 2500;
int lastXC=0;
int lastYC=0;
void setup() {
  fullScreen();
  
  kinect = new KinectPV2(this);
  
  kinect.enableColorImg(true);
  kinect.enableDepthImg(true);
  kinect.init();

  opencv = new OpenCV(this, 1920, 1080);
}

void draw() {
  if((frameCount%20)==0)
  {
    check2=!check2;

    println("frameCount=",frameCount,"x=",mouseX,"y=",mouseY);
  }
  
  int[] colors = kinect.getRawColor();
  int[] depths = kinect.getRawDepthData();
  int convDepths[] = new int [(1440*900)];
  opencv.loadImage(kinect.getColorImage());
  PImage img = opencv.getInput();
  image(img, 0, 0,1440,900);
  img.loadPixels();
   for (int x = 0; x < 1440; x++) 
   {
    for (int y = 0; y < 900; y++) 
    {
      int offset = x + y * 1440;
        
      int c = colors[offset];  
      if (red(c) > minR && red(c) < maxR) 
      {
        noFill();
        stroke(0);
        strokeWeight(6);
        //STOPPED HERE MANE
        int n=6;


        point(x,y);

        lastXC=x;
        lastYC=y;
      } 
    }
   }
   
  for(int i=0;(i <  (  (kinect.WIDTHDepth)*(kinect.HEIGHTDepth)  ) ) && (i*ratio) < (1440*900);i++  )
  {
  convDepths[(i%ratio)*i] = depths[i];
  }

if(check1){




if(mouseX< displayWidth && mouseX>0 && mouseY>0 && mouseY<displayHeight){//Setting mouseoffsetC
  mouseOffsetC = (mouseX + mouseY * 1440);
  if(mouseOffsetC>=(1440*900)){mouseOffsetC=0;}
  }


  
  
if(mouseX< kinect.WIDTHDepth && mouseX>0 && mouseY>0 && mouseY<kinect.HEIGHTDepth){//Setting mouseoffsetD
  mouseOffsetD = (mouseX + mouseY * kinect.WIDTHDepth);
  if(mouseOffsetD>=((kinect.WIDTHDepth)*(kinect.HEIGHTDepth))){mouseOffsetD=0;}
  }  
  
  
  
  fill(0,255,0);
  stroke(0,255,0);
  ellipse(mouseX,mouseY,8,8);
  noFill();
  stroke(0);
  ellipse(mouseX,mouseY,24,24);  
  stroke(0);
  ellipse(mouseX,mouseY,28,28);
  stroke(255,0,0);
  ellipse(mouseX,mouseY,46,46);
  stroke(255,0,0);
  ellipse(mouseX,mouseY,50,50);
  
  println("colors=",colors[mouseOffsetC],"depths=",convDepths[mouseOffsetD*ratio]);
    copy(kinect.getColorImage(),0,0,1920,1080,0,0,200,200);
    img.updatePixels();
}
} 
void mousePressed(){


check1=!check1;

}

void keyPressed() {

  if(key == '1'){
    check3=!check3;
  }

  if(key == '2'){

  }
  if(key == '3'){

  }

  if (key == 'a') {

  }

  if (key == 's') {

  }

  if (key == 'z') {

  }


  if (key == 'x') {

  }
}
