
import cv2
import ktb
import time
import pylibfreenect2 as fn
import os
import sys
from cv2 import rectangle
import numpy as np
logger = fn.createConsoleLogger(fn.LoggerLevel.NONE)
fn.setGlobalLogger(logger)
k = ktb.Kinect(pipeline=fn.OpenGLPacketPipeline())

timer = time.time()

video = cv2.VideoCapture(r'/home/kpc/Desktop/SLP_KINECT/data/pics/VideoCapture24.avi')
frame = k.get_frame(ktb.COLOR)
depth_frame = k.get_frame(ktb.DEPTH)
depths = depth_frame
image = k.get_frame(ktb.COLOR)
cv2.cvtColor(image,cv2.COLOR_RGB2HSV_FULL)
ok = True

def main():
    #recordKinect()
    trackingLive()
def recordKinect():
        currTime = timer
        lastTime = currTime
        currTime = time.time()
        identification = "VideoCapture"

        path = next_path(r'/home/kpc/Desktop/SLP_KINECT/data/pics/'+identification+'%s'+'.avi')


        print("Video Saving to path: "+path+"!!!"+"\n")
        

        colorVideo = k.record(path)
        
def trackingLive():
    
        currTime = timer
        lastTime = currTime
        currTime = time.time()
        identification = "LiveCapture"

        image = k.get_frame(ktb.COLOR)
        #image = cv2.cvtColor(image,cv2.COLOR_RGB2HSV_FULL)
        colors = cv2.cvtColor(image,cv2.COLOR_RGB2HSV_FULL)

        tracker = initializeTracker(colors)    
        print("==> Tracking is started. Press 'SPACE' to re-initialize tracker or 'ESC' for exit...")


        while True:
            # Specify as many types as you want here
            depth_frame = k.get_frame(ktb.DEPTH)
            frame = k.get_frame(ktb.COLOR)
            depths = depth_frame
            image = k.get_frame(ktb.COLOR)
            colors = cv2.cvtColor(image,cv2.COLOR_RGB2HSV_FULL)
            
            


                #if there is a new frame, get the average depth of "ball area" from recorded depth data
            
            ok, newbox = tracker.update(colors)
           
            print(ok, newbox)

            x,y,w,h = newbox
            
            a = (int(x), int(y))
            b = (int(x + w), int(y + h))
    
            midpoint = (int(x + w/2) , int(y + h/2))

            print("Ball Location:", midpoint, depths[midpoint])                 #print real world location of tracked midpoint

            #First rectanglecall
            cv2.circle(img=colors, center = midpoint, radius = int(w / 4), color = (0,0,200))
            cv2.putText(img=colors, text=str(depths[midpoint]), org=a, fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.7, color=(0,200,0), thickness=2)
            cv2.rectangle(img=colors, rec=newbox , color=(0, 0, 200))
            #calls two cv2 methods which shouldn't influence rectangle
 
            cv2.imshow("tracking", colors)                #VIDEO AND TRACKER UPDATED HERE
            
            #cv2.imshow('frame', frame)
            cv2.setMouseCallback('tracking', mouseRGB)

            kk = cv2.waitKey(1)
            if kk == 32:  # SPACE
                tracker = initializeTracker(colors)
            if kk == 27:  # ESC
                break
            
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
def mouseRGB(event,x,y,flags,param):
    if event == cv2.EVENT_LBUTTONDOWN: #checks mouse left button down condition
        depths = depth_frame
        colors = cv2.cvtColor(image,cv2.COLOR_RGB2HSV_FULL)
        
        colorsB = colors[y,x,0]
        colorsG = colors[y,x,1]
        colorsR = colors[y,x,2]
        colors2 = colors[y,x]

        # colorsB = frame[y,x,0]
        # colorsG = frame[y,x,1]
        # colorsR = frame[y,x,2]
        # colors2 = frame[y,x]
        
        print("Coordinates of pixel: X: ",x,"Y: ",y)
        
        print("Red: ", colorsR)
        print("Green: ", colorsG)
        print("Blue: ", colorsB)
        print("Depth: ", depths[y,x])
        
        print("BRG Format: ",colors2)
        

def initializeTracker(img):
    while True:
        print('==> Select object ROI for tracker ...')
        bbox = cv2.selectROI('tracking', img)

        print('ROI: {}'.format(bbox))

        tracker_types = ['BOOSTING', 'MIL','KCF', 'TLD', 'CSRT', 'GOTURN']

        tracker_type = tracker_types[4]

        if tracker_type == tracker_types[0]:
            tracker = cv2.TrackerBoosting_create()
        elif tracker_type == tracker_types[1]:
            tracker = cv2.TrackerMIL_create()
        elif tracker_type == tracker_types[2]:
            tracker = cv2.TrackerKCF_create()
        elif tracker_type == tracker_types[3]:
            tracker = cv2.TrackerTLD_create()
        elif tracker_type == tracker_types[4]:
            tracker = cv2.TrackerCSRT_create()
        elif tracker_type == tracker_types[5]:
            tracker = cv2.TrackerGOTURN_create()
        
        try:
            tracker.init(img, bbox)
        except Exception as e:
            print('Unable to initialize tracker with requested bounding box. Is there any object?')
            print(e)
            print('Try again ...')
            continue

        return tracker




   
            
        


def next_path(path_pattern):
    """
    Finds the next free path in an sequentially named list of files

    e.g. path_pattern = 'file-%s.txt':

    file-1.txt
    file-2.txt
    file-3.txt

    Runs in log(n) time where n is the number of existing files in sequence
    """
    i = 1

    # First do an exponential search
    while os.path.exists(path_pattern % i):
        i = i * 2
        


    # Result lies somewhere in the interval (i/2..i]
    # We call this interval (a..b] and narrow it down until a + 1 = b
    a, b = (i // 2, i)
    while a + 1 < b:
        c = (a + b) // 2 # interval midpoint
        a, b = (c, b) if os.path.exists(path_pattern % c) else (a, c)

    return path_pattern % b  


if __name__ == "__main__":

            # if (cv2.waitKey(1) & 0xFF == ord('q')):    
            #     time.sleep(1)        
            #     k.stop()
            #     time.sleep(2) 
            #     k.close()
            #     time.sleep(2) 
            #     sys.exit(0) 
            #else:
                main()
 
